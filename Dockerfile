# Fase de contrucción  Creamos el un entorno para compilar "node" apps.
FROM tiangolo/node-frontend:10 as build-stage
# Carpeta de trabajo es donde pondra todo lo producido por esta fase
WORKDIR /deploy
# Copiamos el package.json i hacemos el npm install para bajar todos los paquetes
COPY package*.json /deploy/
RUN npm install
# Copiamos el resto de ficheros i hacemos un npm build
COPY ./ /deploy/
RUN npm run build
# Fase construir imagen
FROM nginx:1.15
COPY --from=build-stage /deploy/build/ /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf