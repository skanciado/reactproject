import React from 'react'
import Carousel from 'react-bootstrap/Carousel';

export const Carousel2 = () => {
	return (
		<Carousel className="h-50">
			<Carousel.Item>
				<img
					className="d-block w-100"
					src="./assets/test/first.png"
					alt="First slide"
				/>
				<Carousel.Caption>
					<h3>¿Quiénes somos?</h3>
					<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
				</Carousel.Caption>
			</Carousel.Item>
			<Carousel.Item>
				<img
					className="d-block w-100"
					src="./assets/test/second.png"
					alt="Second slide"
				/>

				<Carousel.Caption>
					<h3>Soluciones</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</Carousel.Caption>
			</Carousel.Item>
			<Carousel.Item>
				<img
					className="d-block w-100"
					src="./assets/test/third.png"
					alt="Third slide"
				/>

				<Carousel.Caption>
					<h3>Servicios</h3>
					<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
				</Carousel.Caption>
			</Carousel.Item>
		</Carousel>
	)
}
