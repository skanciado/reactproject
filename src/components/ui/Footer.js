import React from 'react';
import './ui.css';
import { useTranslation } from 'react-i18next';

export const Footer = () => {

	const { t } = useTranslation(['footer']);
	const { t: tn } = useTranslation(['nav']);

	return (
		<footer className="page-footer font-small blue pt-4 bg-dark text-white mt-5 links">
			<div className="container-fluid text-center text-md-left">
				<div className="row">
					{/*<hr className="clearfix w-100 d-md-none pb-3" />*/}
					<div className="col-md-1 mb-md-0 mb-1"></div>
					<div className="col-md-2 mb-md-0 mb-2">
						<h5 className="text-uppercase">{t('FOOTER_menu')}</h5>
						<ul className="list-unstyled">
							<li><a href="/quienes" >{tn('NAV_aboutUs')}</a></li>
							<li><a href="/soluciones">{tn('NAV_solutions')}</a></li>
							<li><a href="/servicios">{tn('NAV_services')}</a></li>
						</ul>
					</div>
					<div className="col-md-2 mb-md-0 mb-2">
						<h5 className="text-uppercase">{tn('NAV_solutions')}</h5>
						<ul className="list-unstyled">
							<li><a rel="noopener noreferrer" target="_blank" href='https://plm.t-systems-service.com/en/plm-products/collaboration-and-data-exchange/collaborative-engineering-compdm/data-exchange-715042'>COMPDM</a></li>
							<li><a rel="noopener noreferrer" target="_blank" href='https://plm.t-systems-service.com/en/plm-products/integration-and-migration/soa-pdm-erp-integration/pdm-webconnector-715712'>PDM Webconnector</a></li>
							<li><a rel="noopener noreferrer" target="_blank" href='https://plm.t-systems-service.com/en/plm-products/collaboration-and-data-exchange/3d-data-conversion/com-fox-715112'>COM/FOX</a></li>
							<li><a rel="noopener noreferrer" target="_blank" href='https://plm.t-systems-service.com/en/plm-products/integration-and-migration/cad-pdm-integrations/pdm-workbench/pdm-workbench-715542'>PDM Workbench Catia V5</a></li>
							<li><a rel="noopener noreferrer" target="_blank" href='https://plm.t-systems-service.com/en/plm-products/integration-and-migration/cad-pdm-integrations/tci-catia-v6-teamcenter-integration/tci-715466'>TCI</a></li>
							<li><a rel="noopener noreferrer" target="_blank" href='https://plm.t-systems-service.com/en/plm-products/integration-and-migration/cad-pdm-integrations/cmi-catia-teamcenter-integration/cmi-cmi-rii-715386'>CMI/CMI RII</a></li>
							<li><a rel="noopener noreferrer" target="_blank" href='https://plm.t-systems-service.com/en/plm-cloud/overview/engineering-cloud-platform-715796'>PLM Cloud</a></li>
							<li><a href="/virtualErgonomics">Virtual Ergonomics</a></li>
							<li><a href="/editores">3D & 2D Editors</a></li>
						</ul>
					</div>
					<div className="col-md-3 mb-md-0 mb-3">
						<h5 className="text-uppercase">{tn('NAV_services')}</h5>
						<ul className="list-unstyled">
							<li><a href="/solutionDevelopment">Solution Development</a></li>
							<li><a href="/applicationManagement">Application Management</a></li>
							<li><a href="/systemOperations">System Operations</a></li>
							<li><a href="/systemMigrationUpgrade">System Migration & Upgrade</a></li>
							<li><a href="/dataMigrationUploading">Data Migration & Uploading</a></li>
						</ul>
					</div>
					<div className="col-md-3 mb-md-0 mb-3">
						<h5 className="text-uppercase">{t('FOOTER_contact')}</h5>
						<ul className="list-unstyled">
							<li><span>Sancho de Ávila 110, 08018, Barcelona</span></li>
							<li><a href="mailto:gimm.bo@t-systems.com">gimm.bo@t-systems.com</a></li>
							<li><span>+34 93 293 66 24</span></li>
						</ul>
						<br />
						<h5 className="text-uppercase">T-Systems</h5>
						<ul className="list-unstyled">
							<li><a rel="noopener noreferrer" target="_blanck" href="https://www.t-systems.com/de/en/about-t-systems/company/profile">{t('FOOTER_profile')}</a></li>
						</ul>
					</div>
					<div className="col-md-1 mb-md-0 mb-1"></div>
				</div>
			</div>
			<br />
			<div className="container-fluid text-center text-md-left">
				<div className="row">
					<div className="col-md-1 mb-md-0 mb-1"></div>
					<div className="col-md-2 mb-md-0 mb-2">
						<a target="_black" href="https://www.t-systems.com/">
							<img src="./assets/images/T-Systems-Logo_mini.png" alt="Logo T-Systems" />
						</a>
					</div>
					<div className="col-md-6 mb-md-0 mb-6"></div>
					<div className="col-md-2 mb-md-0 mb-2 text-right size-6"><b>LIFE IS FOR SHARING</b></div>
					<div className="col-md-1 mb-md-0 mb-1"></div>
				</div>
			</div>

			<div className="row">
				<div className="col-md-1 mb-md-0 mb-1"></div>
				<div className="col-md-10 mb-md-0 mb-10">
					<hr className="line" />
				</div>
				<div className="col-md-1 mb-md-0 mb-1"></div>
			</div>

			<div className="row">
				<div className="col-md-1 mb-md-0 mb-1"></div>
				<div className="col-md-10 mb-md-0 mb-10">
					<span className="txt-grey">{t('FOOTER_allRight')}</span>
					<span> | </span>
					<a target="_black" href="https://www.t-systems.com/es/es/disclaimer">{t('FOOTER_responsibility')}</a>
					<span> | </span>
					<a target="_black" href="https://www.t-systems.com/es/es/data-privacy">{t('FOOTER_privacy')}</a>
				</div>
				<div className="col-md-1 mb-md-0 mb-1"></div>
			</div>

			<br />
		</footer>
	)
}
