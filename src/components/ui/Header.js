import React from 'react'
import './css.css';

import i18next from 'i18next';
import { useTranslation } from 'react-i18next';

const Header = () => {

	const { t } = useTranslation(['header']);

	return (
		<div className="row">
			<div className="col-md-3 mb-md-0 mb-3">
				<img className="logoHeader" src="./assets/images/T-Systems-Logo.png" alt="Logo T-Systems" />
			</div>
			<div className="col-md-8 mb-md-0 mb-8"></div>
			<div className="col-md-1 mb-md-0 mb-1">
				<select
					value={t('HEADER_lang')}
					name="idiomas"
					className="idiomas float-right"
					onChange={(val) => { i18next.changeLanguage(val.target.value); }}
				>
					<option key="es" value="es">ES</option>
					<option key="fr" value="fr">FR</option>
					<option key="de" value="de">DE</option>
					<option key="en" value="en">EN</option>
				</select>
			</div>
		</div>
	)
}

export default Header;