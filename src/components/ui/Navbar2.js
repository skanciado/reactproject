import React, { Suspense } from 'react';
import { NavLink } from 'react-router-dom';
//import { AuthContext } from '../../auth/AuthContext';
//import { types } from '../../types/types';
import { Navbar, Nav } from 'react-bootstrap';

import { useTranslation } from 'react-i18next';

export const Navbar2 = () => {
	//const { user: { name }, dispatch } = useContext(AuthContext);
	//const history = useHistory();

	//const handleLogout = () => {
	//	history.replace('/');
	//	dispatch({ type: types.logout }); }

	const { t } = useTranslation(['nav']);

	return (
		<Suspense fallback="loading...">
			<Navbar bg="light" expand="lg">
				<Navbar.Brand href="/home">PLM</Navbar.Brand>

				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mr-auto">
						<NavLink
							activeClassName="active"
							className="nav-item nav-link"
							exact
							to="/quienes"
						>{t('NAV_aboutUs')}</NavLink>
						{/*	<NavLink activeClassName="active" className="nav-item nav-link" exact to="/tecnologicas">ÁREAS TECNOLÓGICAS</NavLink> */}
						<NavLink
							activeClassName="active"
							className="nav-item nav-link"
							exact
							to="/soluciones"
						>{t('NAV_solutions')}</NavLink>
						<NavLink
							activeClassName="active"
							className="nav-item nav-link"
							exact
							to="/servicios"
						>{t('NAV_services')}</NavLink>
					</Nav>
				</Navbar.Collapse>
				{/*<div className="navbar-collapse collapse w-100 order-3 dual-collapse2"><ul className="navbar-nav ml-auto"><span className="nav-item nav-link text-info">{name}</span><button	className="nav-item nav-link btn" onClick={handleLogout}>Logout</button></ul></div>*/}
			</Navbar>
		</Suspense>
	);
}