import React from 'react';
//import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';
import { cardData } from '../../data/cardData';

export const CardItem = ({
	datas
}) => {
	return (
		<Card border="secondary" style={{ width: '18rem' }}>
			<Card.Header>{datas.header}</Card.Header>
			<Card.Body>
				<Card.Title>{datas.title}</Card.Title>
				<Card.Text>{datas.news}</Card.Text>
			</Card.Body>
		</Card>
	)
}

CardItem.propType = {
	datas: cardData.isRequired,
}

//CardItem.defaultProps = { Ptitulo: '' }
//export default Card2; // con el import no hace falta llaves.