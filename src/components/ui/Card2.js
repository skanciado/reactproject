import React from 'react';
import { Card } from 'react-bootstrap';

export const Card2 = () => {
	return (
		<Card border="secondary" style={{ width: '18rem' }}>
			<Card.Header>Header</Card.Header>
			<Card.Body>
				<Card.Title>Title</Card.Title>
				<Card.Text>content.</Card.Text>
			</Card.Body>
		</Card>
	)
}
