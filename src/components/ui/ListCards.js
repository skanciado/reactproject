import React from 'react'
//import { cardData } from '../../data/cardData';
import { CardItem } from './CardItem'
import { useTranslation } from 'react-i18next';

export const ListCards = () => {
 
	const { t } = useTranslation(['cards']);
	
/*	const datos = [
		{ header: tn('NAV_'), title: 'Title', news: 'Magna cras aenean nec gravida faucibus morbi habitasse etiam, tincidunt dis lacus purus ullamcorper tristique tortor, mus at nulla porta libero.' },
		{ header: tn(''), title: 'Title', news: 'Lorem ipsum dolor sit amet consectetur adipiscing elit maecenas tortor, sapien per fermentum mattis natoque ad vestibulum tempor, egestas.' },
		{ header: tn(''), title: 'Title', news: 'Aliquet blandit pellentesque nisl interdum, congue nullam risus purus conubia fringilla proin. Neque class himenaeos dignissim lectus nam parturient morbi.' }
	];*/

	let card1 = { header: t('CARD_1_header'), title: t('CARD_1_title'), news: t('CARD_1_news') }
	let card2 = { header: t('CARD_2_header'), title: t('CARD_2_title'), news: t('CARD_2_news') }
	let card3 = { header: t('CARD_3_header'), title: t('CARD_3_title'), news: t('CARD_3_news') }
	let index = 0;

	return (
		<div className="container d-flex justify-content-between mt-5">
			<CardItem key={index++} datas={card1} />
			<CardItem key={index++} datas={card2} />
			<CardItem key={index++} datas={card3} />
		</div>
	)
}


/*
	{ datos.map((value, index) => {
		let datas = value;
		datas.header = t('NAV_');
		datas.title = t('');
		datas.news = t('');
		return <CardItem key={index} datas={value} />; }) }

*/