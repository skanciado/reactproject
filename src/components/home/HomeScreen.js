import React from 'react'
import { Carousel2 } from '../ui/Carousel2'
import { ListCards } from '../ui/ListCards'

export const HomeScreen = () => {
	return (
		<>
			<Carousel2 />
			<ListCards />
		</>
	)
}
