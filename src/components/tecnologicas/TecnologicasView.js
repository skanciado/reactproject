import React from 'react';

export const TecnologicasView = () => {
	return (
		<>
			<br />
			<div className="content">
				<div className="row">
					<div className="col-8">
						<h3 className="mt-5">Solution Development</h3>
						<p className="mt-5">Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</p>
						<a href='/solutionDevelopment'>Find out more</a>
					</div>
					<div className="col-4 text-right">
						<img src=".\assets\tecnologicas\card_solutionDevelopment.jpg"/>
					</div>
				</div>
			</div>

			<hr />
			<div className="content">
				<div className="row">
					<div className="col-4">
						<img src=".\assets\tecnologicas\card_applicationManagement.jpg" />
					</div>
					<div className="col-8">
						<h3 className="mt-5">Application Management</h3>
						<p className="mt-5">Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</p>
						<a href='/applicationManagement'>Find out more</a>
					</div>
				</div>
			</div>

			<hr />
			<div className="content">
				<div className="row">
					<div className="col-8">
						<h3 className="mt-5">System Operations</h3>
						<p className="mt-5">Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</p>
						<a href='/systemOperations'>Find out more</a>
					</div>
					<div className="col-4 text-right">
						<img src=".\assets\tecnologicas\card_systemOperations.jpg" />
					</div>
				</div>
			</div>
			<hr />
			<div className="content">
				<div className="row">
					<div className="col-4">
						<img src=".\assets\tecnologicas\card_systemMigrationUpgrade.jpg" />
					</div>
					<div className="col-8">
						<h3 className="mt-5">System Migration & Upgrade</h3>
						<p className="mt-5">Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</p>
						<a href='/systemMigrationUpgrade'>Find out more</a>
					</div>
				</div>
			</div>
			<hr />
			<div className="content">
				<div className="row">
					<div className="col-8">
						<h3 className="mt-5">Data Migration & Uploading</h3>
						<p className="mt-5">Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</p>
						<a href='/DataMigrationUploading'>Find out more</a>
					</div>
					<div className="col-4 text-right">
						<img src=".\assets\tecnologicas\card_dataMigrationUploading.jpg" />
					</div>
				</div>
			</div>
			<br />
		</>
	)
}
