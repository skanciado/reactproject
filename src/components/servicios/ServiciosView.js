import React from 'react';
import { CardDeck, Card } from 'react-bootstrap';

export const ServiciosView = () => {
	return (
		<>
			<br />
			<CardDeck>
				<Card>
					<Card.Img variant="top" src=".\assets\tecnologicas\card_solutionDevelopment.jpg" />
					<Card.Body>
						<Card.Title>Solution Development</Card.Title>
						<Card.Text>Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href="/solutionDevelopment">
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>

				<Card>
					<Card.Img variant="top" src=".\assets\tecnologicas\card_applicationManagement.jpg" />
					<Card.Body>
						<Card.Title>Application Management</Card.Title>
						<Card.Text>Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href="/applicationManagement">
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>

				<Card>
					<Card.Img variant="top" src=".\assets\tecnologicas\card_systemOperations.jpg" />
					<Card.Body>
						<Card.Title>System Operations</Card.Title>
						<Card.Text>Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href="/systemOperations">
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>
			</CardDeck>
			<br />
			<CardDeck>
				<Card>
					<Card.Img variant="top" src=".\assets\tecnologicas\card_systemMigrationUpgrade.jpg" />
					<Card.Body>
						<Card.Title>System Migration & Upgrade</Card.Title>
						<Card.Text>Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href="/systemMigrationUpgrade">
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>

				<Card>
					<Card.Img variant="top" src=".\assets\tecnologicas\card_dataMigrationUploading.jpg" />
					<Card.Body>
						<Card.Title>Data Migration & Uploading</Card.Title>
						<Card.Text>Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href="/DataMigrationUploading">
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>
			</CardDeck>
		</>
	)
}
