import React from 'react';
import { CardDeck, Card } from 'react-bootstrap';

//import './soluciones.css';

export const SolucionesView = () => {
	return (
		<>
			<br />
			<CardDeck>
				<Card>
					<Card.Img variant="top" src=".\assets\soluciones\card_COMPDM.jpg" />
					<Card.Body>
						<Card.Title>COMPDM</Card.Title>
						<Card.Text>Engineering Data Exchange for PLM and CAD Data.</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href='https://plm.t-systems-service.com/en/plm-products/collaboration-and-data-exchange/collaborative-engineering-compdm/data-exchange-715042'>
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>

				<Card>
					<Card.Img variant="top" src=".\assets\soluciones\card_PDM_WebConnector.jpg" />
					<Card.Body>
						<Card.Title>PDM WEBCONNECTOR</Card.Title>
						<Card.Text>Corporate solutions for web-integration and web-enabling of existent PDM and Legacy systems are an essential part of the companywide IT integration.</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href='https://plm.t-systems-service.com/en/plm-products/integration-and-migration/soa-pdm-erp-integration/pdm-webconnector-715712'>
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>

				<Card>
					<Card.Img variant="top" src=".\assets\soluciones\card_COMFOX.jpg" />
					<Card.Body>
						<Card.Title>COM/FOX</Card.Title>
						<Card.Text>With the solution COM/FOX, you are able to convert and optimize CAD data. This derived 3D data can be used for variety cases within the development process.</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href='https://plm.t-systems-service.com/en/plm-products/collaboration-and-data-exchange/3d-data-conversion/com-fox-715112'>
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>
			</CardDeck>
			<br />
			<CardDeck>
				<Card>
					<Card.Img variant="top" src=".\assets\soluciones\card_PDM_WORKBENCH.jpg" />
					<Card.Body>
						<Card.Title>PDM WORKBENCH CATIA V5</Card.Title>
						<Card.Text>The PDM Workbench is a CATIA V5 integration which comes with a PDM client integrated in the CATIA V5 GUI. It combines the control of PDM with the CAD superiority of CATIA V5 in a single user ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href='https://plm.t-systems-service.com/en/plm-products/integration-and-migration/cad-pdm-integrations/pdm-workbench/pdm-workbench-715542'>
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>

				<Card>
					<Card.Img variant="top" src=".\assets\soluciones\card_TCI.jpg" />
					<Card.Body>
						<Card.Title>TCI </Card.Title>
						<Card.Text>The 3DEXPERIENCE CATIA V6 Teamcenter Integration (TCI) is a comprehensive solution for all kinds of integration scenarios. The solution integrates the 3DEXPERIENCE virtual product structure to the ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href='https://plm.t-systems-service.com/en/plm-products/integration-and-migration/cad-pdm-integrations/tci-catia-v6-teamcenter-integration/tci-715466'>
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>

				<Card>
					<Card.Img variant="top" src=".\assets\soluciones\card_CMI.jpg" />
					<Card.Body>
						<Card.Title>CMI/CMI RII</Card.Title>
						<Card.Text>CMI is the T-Systems high-end solution for the integration of the CAD systems CATIA V4/V5 with the PDM system Teamcenter Enterprise. The CATIA integration product combines the PLM power of ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href='https://plm.t-systems-service.com/en/plm-products/integration-and-migration/cad-pdm-integrations/cmi-catia-teamcenter-integration/cmi-cmi-rii-715386'>
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>
			</CardDeck>
			<br />
			<CardDeck>
				<Card>
					<Card.Img variant="top" src=".\assets\soluciones\card_PLM_Cloud.jpg" />
					<Card.Body>
						<Card.Title>PLM CLOUD</Card.Title>
						<Card.Text>Manufacturing Industry companies need to adjust their PLM-related business processes flexibly to market demands. New products need to be developed in shorter periods of time.</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							target="_blank"
							href='https://plm.t-systems-service.com/en/plm-cloud/overview/engineering-cloud-platform-715796'>
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>

				<Card>
					<Card.Img variant="top" src=".\assets\soluciones\card_default.jpg" />
					<Card.Body>
						<Card.Title>VIRTUAL ERGONOMICS</Card.Title>
						<Card.Text>Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							href='/virtualErgonomics'>
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>

				<Card>
					<Card.Img variant="top" src=".\assets\soluciones\card_default.jpg" />
					<Card.Body>
						<Card.Title>3D & 2D EDITORS</Card.Title>
						<Card.Text>Lorem ipsum dolor sit amet consectetur adipiscing elit duis semper elementum, fermentum proin tempor etiam fringilla lacinia nec sollicitudin vehicula ...</Card.Text>
					</Card.Body>
					<Card.Footer>
						<Card.Link
							href='/editores'>
							Find out more
							</Card.Link>
					</Card.Footer>
				</Card>
			</CardDeck>
		</>
	)
}