import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Header from '../components/ui/Header';
import { Navbar2 } from '../components/ui/Navbar2';
import { ServiciosView } from '../components/servicios/ServiciosView';
import { SolucionesView } from '../components/soluciones/SolucionesView';
//import { TecnologicasView } from '../components/tecnologicas/TecnologicasView';
import { QuienesView } from '../components/quienes/QuienesView';
import { HomeScreen } from '../components/home/HomeScreen';
import { EditorView } from '../components/soluciones/EditoresView';
import { VirtualErgonomicsView } from '../components/soluciones/VirtualErgonomicsView';
import { SolutionDevelopmentView } from '../components/tecnologicas/SolutionDevelopmentView';
import { ApplicationManagementView } from '../components/tecnologicas/ApplicationManagementView';
import { SystemOperationsView } from '../components/tecnologicas/SystemOperationsView';
import { SystemMigrationUpgradeView } from '../components/tecnologicas/SystemMigrationUpgradeView';
import { DataMigrationUploadingView } from '../components/tecnologicas/DataMigrationUploadingView';
import { Footer } from '../components/ui/Footer';

import './routes.css';

export const DashboardRoutes = () => {
	return (
		<div id="pagePrimary">
			<div className="container-fluid text-center text-md-left">
				<div className="row router-header">
					<div className="col-md-1 mb-md-0 mb-1"></div>
					<div className="col-md-10 mb-md-0 mb-10">
						<Header />
					</div>
					<div className="col-md-1 mb-md-0 mb-1"></div>
				</div>
				<div className="row router-navbar">
					<div className="col-md-1 mb-md-0 mb-1"></div>
					<div className="col-md-10 mb-md-0 mb-10">
						<Navbar2 />
					</div>
					<div className="col-md-1 mb-md-0 mb-1"></div>
				</div>
			</div>
			<div className="container mt-2">
				<Switch>
					<Route exact path="/servicios" component={ServiciosView} />
					<Route exact path="/soluciones" component={SolucionesView} />
					{/*<Route exact path="/tecnologicas" component={TecnologicasView} />*/}
					<Route exact path="/quienes" component={QuienesView} />
					<Route exact path="/editores" component={EditorView} />
					<Route exact path="/virtualErgonomics" component={VirtualErgonomicsView} />
					<Route exact path="/solutionDevelopment" component={SolutionDevelopmentView} />
					<Route exact path="/applicationManagement" component={ApplicationManagementView} />
					<Route exact path="/systemOperations" component={SystemOperationsView} />
					<Route exact path="/systemMigrationUpgrade" component={SystemMigrationUpgradeView} />
					<Route exact path="/dataMigrationUploading" component={DataMigrationUploadingView} />
					<Route exact path="/home" component={HomeScreen} />
					<Redirect to="/home" />
				</Switch>
			</div>

			<Footer />
		</div>
	);
}
